# Maze Game

Game made for the miniquest of the course of Software Project.

## Usage

### Compile
```bash
g++ main.cpp -I include/maze.cpp src/maze.cpp
```

### Run
```bash
./a.out
```

### Controls

To move the Hero `H` use the `[W]` `[A]` `[S]` `[D]` keys press `[ENTER]` to confirm the move, or press `[Q]` to quit the game.