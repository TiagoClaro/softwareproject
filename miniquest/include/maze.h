#pragma once
#include <iostream>
#include <cstdlib> 
#include <ctime>

namespace logic {

    class maze{
        public:
            maze();

            int getRow();

            int getColumn();


            void move();

            void printMaze();

        private:
            bool key=false;
            std::string label = "";
            int keypos[2];
            int row;
            int column;
            int lastRow;
            int lastColumn;
            char maze_map[10][11] = {
                {'X','X','X','X','X','X','X','X','X','X',' '},
                {'X',' ',' ',' ',' ',' ',' ',' ',' ','X',' '},
                {'X',' ','X','X',' ','X',' ','X',' ','X',' '},
                {'X','D','X','X',' ','X',' ','X',' ','X',' '},
                {'X',' ','X','X',' ','X',' ','X',' ','X',' '},
                {'X',' ',' ',' ',' ',' ',' ','X',' ','E',' '},
                {'X',' ','X','X',' ','X',' ','X',' ','X',' '},
                {'X',' ','X','X',' ','X',' ','X',' ','X',' '},
                {'X',' ','X','X',' ',' ',' ',' ',' ','X',' '},
                {'X','X','X','X','X','X','X','X','X','X',' '}
            };

            std::string itemWallColider();

            void setKey();
    };
}

namespace cli{

    class cli_io{

        public:

            void ReadInput();

        private:
            
            std::string input;

    };
}