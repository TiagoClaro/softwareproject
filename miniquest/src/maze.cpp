#include "../include/maze.h"
#include <iostream>
#include <cstdlib> 
#include <ctime>

#define KEY_W "w"
#define KEY_S "s"
#define KEY_A "a"
#define KEY_D "d"

logic::maze::maze(){
    row = 1;
    column = 1;
    key = false;
    setKey();
}

int  logic::maze::getRow(){
    return row;
}

int logic::maze::getColumn(){
    return column;
}


void logic::maze::move(){
    std::string keyInput;
    std::cout << "Press 'w','a','s','d' to move" <<std::endl << "or 'q' to quit" <<std::endl;
    std::cin >> keyInput;

    lastColumn = column;
    lastRow = row;

    while(keyInput != KEY_W && keyInput != KEY_A && keyInput != KEY_S && keyInput != "q" && keyInput != KEY_D){
        std::cout << "-------Invalid key-------" << std::endl;
        std::cout << "Press 'w','a','s','d' to move" << std::endl <<  "or 'q' to quit:" << std::endl;
        std::cin >> keyInput;
    }

    if(keyInput == KEY_W){
        row--;
    }
    else if(keyInput == KEY_A){
        column--;
    }
    else if(keyInput == KEY_S){
        row++;
    }
    else if(keyInput == KEY_D){
        column++;
    }
    else if(keyInput == "q"){
        exit(0);
    }

    label = itemWallColider();

}

void logic::maze::printMaze() {

    std::cout << label << std::endl;

    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 11; j++){
            if(i==row && j==column){
                std::cout << "H ";
            }
            else if(i==keypos[0] && j==keypos[1]){
                if(!key){
                    std::cout << "K ";
                    maze_map[i][j] = 'K';
                }
                else{
                    std::cout << "  ";
                    maze_map[i][j] = ' ';
                }
            }
            else{
                std::cout << maze_map[i][j] << " ";
            }
        }

        std::cout << std::endl;
    }

    if(label == "You died! It's Game Over" || label == "You escaped!"){
        exit(0);
    }

}

std::string logic::maze::itemWallColider(){

    label.clear();

    if(maze_map[row][column] == 'X'){
        std::cout << "You can't go there!" << std::endl;
        row = lastRow;
        column = lastColumn;

        return "You can't go there!";
    }
    else if(maze_map[row][column] == 'K'){
        std::cout << "You got the key!" << std::endl;
        maze_map[row][column] = ' ';
        key = true;

        return "You got the key!";

    }
    else if(maze_map[row][column] == 'D' || maze_map[row+1][column] == 'D' || maze_map[row-1][column] == 'D' || maze_map[row][column+1] == 'D' || maze_map[row][column-1] == 'D'){
        std::cout << "You died! It's Game Over" << std::endl;

        return "You died! It's Game Over";
    }
    else if(maze_map[row][column] == 'E'){
        if(key == 0){
            std::cout << "You need the key to escape!" << std::endl;
            row = lastRow;
            column = lastColumn;

            return "You need the key to escape!";
        }
        else{
            std::cout << "You opened the door!" << std::endl;
            maze_map[row][column] = ' ';

            return "You opened the door!";
        }
    }
    else if(row == 5 && column == 10){
        std::cout << "You escaped!" << std::endl;

        return "You escaped!";
    }

    return "";
}

void logic::maze::setKey(){

    std::srand(static_cast<unsigned int>(std::time(nullptr)));
            
    do{
        keypos[0] = rand() % 10;
        keypos[1] = rand() % 10;
    } while (maze_map[keypos[0]][keypos[1]] != ' ' || (keypos[0] != row && keypos[1] != column) || keypos[1] == 11);
    
}

void cli::cli_io::ReadInput(){
    std::string input;
    std::cin >> input;
}