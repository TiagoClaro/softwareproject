#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>


int main() {
	
	sf::RenderWindow window(sf::VideoMode(1600, 900), "SFML works!", sf::Style::Default);
	sf::Event event;

	while(window.isOpen()){

		while(window.pollEvent(event)){
			if(event.type == sf::Event::Closed){
				window.close();
			}
			if(event.type == sf::Event::KeyPressed){
				if(event.key.code == sf::Keyboard::Escape){
					window.close();
				}
			}
		}

		window.clear();
		window.display();
	}

	return 0;
}