# Software Project

Software Project developed by **Tiago Claro**, **João Oliveira** and **Sofia Silva** for the course of Electrical Engineering at University of Porto.

## Miniquest

Maze game developed in the first classes of the course.

## MainProject

Main project of the course, a game developed in C++.